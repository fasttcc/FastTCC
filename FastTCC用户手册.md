# FastTCC用户手册

[TOC]

## 准备工作

### 创建日志表

需要使用TCC的场景，无论是消费者还是服务的提供者，数据库需要新增一张供FastTCC使用的日志表。建表语句为

```sql
create table fasttcc
(
	global_id varchar(100) not null,
	branch_id varchar(100) not null,
	create_time bigint not null,
	identifier varchar(50) null
);
create index jfiretcc_global_id_idx
	on fasttcc (global_id);
```

### 引入依赖

目前FastTCC支持RestTemplate和Feign两种远端调用方式，根据项目中使用的远端调用方式选择不同的依赖：

RestTemplate：

```xml
<dependency>
  <groupId>com.bosssoft.platform</groupId>
  <artifactId>fasttcc-support-resttemplate</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>
```

Feign:

```xml
<dependency>
  <groupId>com.bosssoft.platform</groupId>
  <artifactId>fasttcc-support-feign</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>
```

### 配置数据源

使用FastTCC对数据源的配置有一个命名的要求，要将数据源的Bean命名为`realDataSource`，例如在Spring中可以使用配置类配置，参考代码如下

```java
@Configuration
public class DatasourceProvider
{
    @Bean("realDataSource")
    public DataSource dataSource()
    {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        dataSource.setUrl("jdbc:mysql://localhost:3306/inst01?characterEncoding=utf8&useSSL=false");
        dataSource.setDriverClassName(Driver.class.getName());
        return dataSource;
    }
}
```

### 配置启动注解

如果是使用Feign的话，在SpringBoot的启动类上配置如下注解：

```java
@EnableTcc
@EnableTccFeignSupport
```

如果是使用RestTemplate的话，在SpringBoot的启动类上配置如下注解

```java
@EnableTcc
@EnableRestTemplateSupport
```

## 代码开发

### 概述

TCC模式下，需要针对一个接口开发三个实现，分别是尝试，确认，取消。

以例子说明，假设有一个转账接口和三个对应的实现

```java
interface Transfer{
    void transfer(String account1,int amount)
}
class TryTransfer implements Transfer{
    public void transfer(String accountId,int amount)
    {
		   jdbctemplate.update("update account set amount = amount-?,froze=froze+? where accountId=?",amount,amount,accountId);
    }
}
class ConfirmTransfer implements Transfer
{
   public void transfer(String accountId,int amount)
    {
		   jdbctemplate.update("update account set froze=froze-? where accountId=?",amount,accountId);
    }
}
class CancelTransfer implements Transfer
{
public void transfer(String accountId,int amount)
    {
		   jdbctemplate.update("update account set amount = amount+?,froze=froze-? where accountId=?",amount,amount,accountId);
    }
}
```

在调用接口的尝试实现完成后，如果方法正常结束，FastTCC会自动执行确认实现的对应方法；如果方法抛出异常，FastTCC会自动执行取消实现的对应的方法。

### 开始使用

FastTCC需要通过注解的方式来确定项目中有哪些接口方法是需要执行TCC模式的。在需要执行TCC模式的接口方法的实现上打上`@Tcc`注解，如下

```java
class TryTransfer implements Transfer{
    @Tcc(tccInterface = Transfer.class, confirmClass = ConfirmTransfer.class, cancelClass = CancelTransfer.class)
    public void transfer(String accountId,int amount)
    {
		   jdbctemplate.update("update account set amount = amount-?,froze=froze+? where accountId=?",amount,amount,accountId);
    }
}
```

其中ConfirmClass和CancelClass的值不是必须的。如果没有对应的步骤或者不需要的话，留空即可。框架会忽略对应的步骤。

### 简单模式

FastTCC还支持将尝试，确认，取消三个方法写在同一个类内，即所谓简单模式。使用注解`@TccSimple`实现，参考示例代码

```java
    @Transactional
    @TccSimple(confirmMethod = "confirm", cancelMethod = "cancel")
    public void simpleTransfer(User user)
    {
        amountOp.deac();
        String result = providerFeignClient.add();
    }
    public void confirm(User user)
    {
        confirmTransfer.transfer(user);
    }
    public void cancel(User user)
    {
        cancelTransfer.transfer(user);
    }
```

简单模式下，要求确认方法和取消方法的方法名能够在同一个类下找到，并且三个方法的入参类型需要完全一致。

### 幂等保证

FastTCC框架保证对确认实现或者取消实现必然会调用且仅会调用一次。因此业务层不必在这两个实现上做幂等处理。