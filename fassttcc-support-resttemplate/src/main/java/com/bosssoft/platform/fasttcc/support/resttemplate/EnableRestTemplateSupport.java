package com.bosssoft.platform.fasttcc.support.resttemplate;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Import({ConfigRestTempleteInterceptor.class})
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableRestTemplateSupport
{
}
