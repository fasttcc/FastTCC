package com.bosssoft.platform.fasttcc.support.spring;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(value = RetentionPolicy.RUNTIME)
@Import({SpringTccConfiguration.class})
public @interface EnableTcc
{
}
