package com.bosssoft.platform.fasttcc.support.spring;

import com.bosssoft.platform.fasttcc.Xid;
import com.bosssoft.platform.fasttcc.impl.XidImpl;
import com.bosssoft.platform.fasttcc.rpc.command.TccCommandHandler;
import com.bosssoft.platform.fasttcc.rpc.command.TccCommitCommand;
import com.bosssoft.platform.fasttcc.rpc.command.TccRollbackCommand;
import com.jfireframework.baseutil.StringUtil;
import com.jfireframework.baseutil.TRACEID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RequestMapping
@RestController
public class TccController
{
    static final         String            NODEIDENTIFIER = "FASTTCC-NODEIDENTIFIER";
    private static final Logger            LOGGER         = LoggerFactory.getLogger(TccController.class);
    @Resource
    private              TccCommandHandler tccCommandHandler;

    @RequestMapping("/fasttcc/commit/{globalId}")
    public String commit(@PathVariable("globalId") String globalId, HttpServletRequest request)
    {
        String           traceId       = TRACEID.newTraceId();
        TccCommitCommand commitCommand = new TccCommitCommand();
        String           identifier    = request.getHeader(NODEIDENTIFIER);
        Xid              xid           = new XidImpl();
        ((XidImpl) xid).setGlobalId(StringUtil.hexStringToBytes(globalId));
        commitCommand.setXid(xid);
        commitCommand.setNodeIdentifier(identifier);
        LOGGER.debug("traceId:{} 收到远端TCC确认请求，xid:{},上游节点标识:{}", traceId, xid, identifier);
        try
        {
            tccCommandHandler.processTccCommitCommand(commitCommand);
            LOGGER.debug("traceId:{} tcc确认请求执行成功", traceId);
            return "success";
        }
        catch (Exception e)
        {
            LOGGER.error("traceId:{} tcc确认请求执行异常", traceId, e);
            return "fail";
        }
    }

    @RequestMapping("/fasttcc/rollback/{globalId}")
    public String rollback(@PathVariable("globalId") String globalId, HttpServletRequest request)
    {
        String             traceId    = TRACEID.newTraceId();
        TccRollbackCommand command    = new TccRollbackCommand();
        String             identifier = request.getHeader(NODEIDENTIFIER);
        Xid                xid        = new XidImpl();
        ((XidImpl) xid).setGlobalId(StringUtil.hexStringToBytes(globalId));
        command.setXid(xid);
        command.setNodeIdentifier(identifier);
        LOGGER.debug("traceId:{} 收到远端TCC回滚请求，xid:{},上游节点标识:{}", traceId, xid, identifier);
        try
        {
            tccCommandHandler.processTccRollbackCommand(command);
            LOGGER.debug("traceId:{} tcc回滚请求执行成功", traceId);
            return "success";
        }
        catch (Exception e)
        {
            LOGGER.error("traceId:{} tcc回滚请求执行异常", traceId, e);
            return "fail";
        }
    }
}
