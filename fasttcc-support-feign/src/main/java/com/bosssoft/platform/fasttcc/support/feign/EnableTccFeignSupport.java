package com.bosssoft.platform.fasttcc.support.feign;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Import(FeignClientPostProcessor.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableTccFeignSupport
{
}
