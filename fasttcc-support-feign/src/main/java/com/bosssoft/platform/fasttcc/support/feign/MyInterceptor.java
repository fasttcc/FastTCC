package com.bosssoft.platform.fasttcc.support.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MyInterceptor implements RequestInterceptor
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MyInterceptor.class);

    @Override
    public void apply(RequestTemplate requestTemplate)
    {
        LOGGER.debug("自定义拦截");
        LOGGER.debug("{}",requestTemplate.url());
        LOGGER.debug("{}",requestTemplate.request().url());

    }
}
