package com.bosssoft.platform.fasttcc;

public interface CompleteStageListener
{
    void onRemoteFinish(RemoteResource remoteResource, boolean completed);

    void onFinish();
}
