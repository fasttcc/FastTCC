package com.bosssoft.platform.fasttcc;

public interface LocalTransaction
{
    int preLocalTxIndex();

    int txIndex();

    Xid xid();

    boolean isCommited();

    boolean isRollbacked();

    void setPreLocalTransaction(LocalTransaction pred);

    void setIndex(int index);

    void setCommited();

    void setRollbacked();
}
