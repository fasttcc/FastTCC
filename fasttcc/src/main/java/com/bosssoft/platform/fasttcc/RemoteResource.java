package com.bosssoft.platform.fasttcc;

public interface RemoteResource
{
    boolean isSameInstance(RemoteResource remoteResource);

    void commit(boolean async, Xid xid, CompleteStageListener listener);

    void rollback(boolean async, Xid xid, CompleteStageListener listener);

    String getIdentifier();
}
