package com.bosssoft.platform.fasttcc;

public interface RemoteResourceFactory
{
    /**
     * 标识符要求必须为${ip}:${port}:${value}，其中value的部分可以自行定义
     * @param identifier
     * @return
     */
    RemoteResource get(String identifier);
}
