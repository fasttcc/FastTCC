package com.bosssoft.platform.fasttcc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 同样赋值的情况下，Class优先
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Tcc
{
    Class<?> confirmClass() default Object.class;

    Class<?> cancelClass() default Object.class;

    String confirmBeanName() default "";

    String cancelBeanName() default "";
}
