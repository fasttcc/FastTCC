package com.bosssoft.platform.fasttcc;

import com.bosssoft.platform.fasttcc.rpc.command.TccCommandHandler;
import com.bosssoft.platform.fasttcc.support.BeanClassFinder;
import com.bosssoft.platform.fasttcc.support.CompleteStageHelper;

import javax.sql.DataSource;

public interface TccConfiguration
{
    TccTransactionManager build();

    void setTccLogger(TccLogger tccLogger);

    void setTccOperationRegistry(TccOperationRegistry registry);

    void setBeanClassFinder(BeanClassFinder beanClassFinder);

    void setXidFactory(XidFactory xidFactory);

    void setIdentifier(String identifier);

    void setRemoteResourceFactory(RemoteResourceFactory factory);

    void setTccCommandHandler(TccCommandHandler tccCommandHandler);

    void setCompleteStageHelper(CompleteStageHelper completeStageHelper);

    void setDataSource(DataSource dataSource);
}
