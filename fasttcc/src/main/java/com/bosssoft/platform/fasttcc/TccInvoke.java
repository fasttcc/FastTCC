package com.bosssoft.platform.fasttcc;

public interface TccInvoke
{
    Object[] getParams();

    TccOperation getTccOperation();

    Xid getCompleteStageXid();

    LocalTransaction getAssociatedLocalTransaction();

    void commit(boolean async, CompleteStageListener listener);

    void rollback(boolean async, CompleteStageListener listener);

    void associateLocalTransaction(LocalTransaction localTransaction);

    boolean isCompleted();

    void markCompleted();
}
