package com.bosssoft.platform.fasttcc;

import com.bosssoft.platform.fasttcc.rpc.command.TccCommandHandler;

import javax.sql.DataSource;

public interface TccLogger
{
    void createTccTransaction(TccTransaction transaction);

    void registerLocalTransaction(LocalTransaction localTransaction);

    void registerTccInvoke(TccInvoke tccInvoke, TccTransaction tccTransaction);

    void registerRemoteResource(RemoteResource remoteResource, Xid xid);

    void updateTccTransactionState(TccTransaction transaction);

    void recover(TccTransactionManager tccTransactionManager, TccCommandHandler tccCommandHandler);
}
