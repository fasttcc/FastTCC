package com.bosssoft.platform.fasttcc;

import java.lang.reflect.Method;

public interface TccOperation
{
    Method getTryMethod();

    Method getConfirmMethod();

    Method getCancelMethod();

    Class<?> getTryClass();

    Class<?> getConfirmClass();

    Class<?> getCancelClass();
}
