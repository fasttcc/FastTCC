package com.bosssoft.platform.fasttcc;

import java.lang.reflect.Method;

public interface TccOperationRegistry
{
    TccOperation get(Method method);

}
