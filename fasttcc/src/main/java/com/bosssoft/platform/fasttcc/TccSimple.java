package com.bosssoft.platform.fasttcc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TccSimple
{
    String confirmMethod() default "";

    String cancelMethod() default "";
}
