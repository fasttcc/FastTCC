package com.bosssoft.platform.fasttcc;

public interface TccTransaction
{
    byte COORDINATOR  = 1;
    byte PARTICIPATOR = 2;

    int ACTIVE            = 1;
    int MARK_FOR_COMMIT   = 2;
    int MARK_FOR_ROLLBACK = 3;
    int FINISHED          = 4;

    Xid getCurrentLocalTransactionXid();

    boolean isFirstLocalTransactionCommited();

    byte role();

    void commitCurrentLocalTransaction();

    void rollbackCurrentLocalTransaction();

    /**
     * 该方法可以安全的重试
     */
    void processCompleteStage();

    int getState();

    void markForCommit();

    void markForRollback();

    String getPropagatedBy();

    Xid getXid();
}
