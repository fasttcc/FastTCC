package com.bosssoft.platform.fasttcc;

import java.lang.reflect.Method;
import java.util.List;

public interface TccTransactionManager
{
    TccTransaction getCurrentTccTransaction();

    void associateTccTransaction(TccTransaction transaction);

    /**
     * 将当前关联的TCC事务解除绑定
     *
     * @return
     */
    TccTransaction deAssociateTccTransaction();

    /**
     * 新建一个协调者角色的TCC事务，该TCC事务会自动绑定到当前线程。
     */
    TccTransaction newCoordinatorTccTransaction();

    /**
     * 使用传入的xid新建一个参与者TCC事务。
     *
     * @param xid
     * @return
     */
    TccTransaction newParticipatorTccTransaction(Xid xid, String propagatedBy);

    void registerRemoteResource(TccTransaction tccTransaction, String remoteIdentifier);

    void registerLocalTransaction(TccTransaction tccTransaction);

    void registerTccInvoke(Method method, Object[] params, TccTransaction tccTransaction);

    /**
     * 返回使用该事务管理器的应用的标识符
     *
     * @return
     */
    String getIdentifier();

    void addReCompleteTransaction(TccTransaction transaction);

    /**
     * 使用指定参数，构建一个TCC事务对象
     *
     * @param xid
     * @param role
     * @param state
     * @return
     */
    TccTransaction reConstruct(Xid xid, int role, int state, String propagatedBy);

    LocalTransaction reConstructLocalTransaction(TccTransaction tccTransaction, byte[] branchId, int preTxIndex, int txIndex);

    TccInvoke reConstructTccInvoke(TccTransaction tccTransaction, Object[] paramArray, Method tccMethod, LocalTransaction localTransaction, byte[] completeStageXidBranchId);

    RemoteResource reConstructRemoteResource(TccTransaction tccTransaction, String identifier);

    void resetLocalTransactions(TccTransaction tccTransaction, List<LocalTransaction> localTransactions);

    void resetRemoteResources(TccTransaction tccTransaction, List<RemoteResource> remoteResources);

    void setsetTccInvokes(TccTransaction tccTransaction, List<TccInvoke> tccInvokes);
}
