package com.bosssoft.platform.fasttcc;

public interface Xid
{
    /**
     * 获取全局ID,8个字节
     *
     * @return
     */
    byte[] getGlobalId();

    /**
     * 获取分支ID，8个字节
     *
     * @return
     */
    byte[] getBranchId();
}
