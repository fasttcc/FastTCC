package com.bosssoft.platform.fasttcc;

public interface XidFactory
{
    Xid createGlobalID();

    Xid createGlobalID(byte[] globalId);

    Xid createBranchId(byte[] globalId);

    Xid createXid(byte[] globalId, byte[] branchId);
}
