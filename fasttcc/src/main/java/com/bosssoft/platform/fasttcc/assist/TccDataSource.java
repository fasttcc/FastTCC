package com.bosssoft.platform.fasttcc.assist;

import com.bosssoft.platform.fasttcc.TccTransactionManager;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class TccDataSource implements DataSource
{
    private DataSource            physicalDs;
    private TccTransactionManager tccTransactionManager;

    public DataSource getPhysicalDs()
    {
        return physicalDs;
    }

    public void setPhysicalDs(DataSource physicalDs)
    {
        this.physicalDs = physicalDs;
    }

    public TccTransactionManager getTccTransactionManager()
    {
        return tccTransactionManager;
    }

    public void setTccTransactionManager(TccTransactionManager tccTransactionManager)
    {
        this.tccTransactionManager = tccTransactionManager;
    }

    @Override
    public Connection getConnection() throws SQLException
    {
        return new TccConnection(physicalDs.getConnection(), tccTransactionManager);
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException
    {
        return new TccConnection(physicalDs.getConnection(username, password), tccTransactionManager);
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException
    {
        return physicalDs.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException
    {
        return physicalDs.isWrapperFor(iface);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException
    {
        return physicalDs.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException
    {
        physicalDs.setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException
    {
        physicalDs.setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException
    {
        return physicalDs.getLoginTimeout();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException
    {
        return physicalDs.getParentLogger();
    }
}
