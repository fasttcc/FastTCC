package com.bosssoft.platform.fasttcc.impl;

import com.bosssoft.platform.fasttcc.LocalTransaction;
import com.bosssoft.platform.fasttcc.Xid;

public class LocalTransactionImpl implements LocalTransaction
{
    private       int     preLocalTxIndex = -1;
    private       int     txIndex         = -1;
    private final Xid     xid;
    private       boolean commited        = false;
    private       boolean rollbacked      = false;

    public LocalTransactionImpl(Xid xid)
    {
        this.xid = xid;
    }

    @Override
    public int preLocalTxIndex()
    {
        return preLocalTxIndex;
    }

    @Override
    public int txIndex()
    {
        return txIndex;
    }

    @Override
    public Xid xid()
    {
        return xid;
    }

    @Override
    public boolean isCommited()
    {
        return commited;
    }

    @Override
    public boolean isRollbacked()
    {
        return rollbacked;
    }

    @Override
    public void setPreLocalTransaction(LocalTransaction pred)
    {
        if (pred == null)
        {
            preLocalTxIndex = -1;
        }
        else
        {
            preLocalTxIndex = pred.txIndex();
        }
    }

    @Override
    public void setIndex(int index)
    {
        this.txIndex = index;
    }

    @Override
    public void setCommited()
    {
        commited = true;
    }

    @Override
    public void setRollbacked()
    {
        rollbacked = true;
    }

    public void setPreLocalTxIndex(int preLocalTxIndex)
    {
        this.preLocalTxIndex = preLocalTxIndex;
    }
}
