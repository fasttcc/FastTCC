package com.bosssoft.platform.fasttcc.impl;

import com.bosssoft.platform.fasttcc.TccOperation;

import java.lang.reflect.Method;

public class TccOperationImpl implements TccOperation
{
    private Method   tryMethod;
    private Method   cancelMethod;
    private Method   confirmMethod;
    private Class<?> tryClass;
    private Class<?> confirmClass;
    private Class<?> cancelClass;

    @Override
    public Method getTryMethod()
    {
        return tryMethod;
    }

    public void setTryMethod(Method tryMethod)
    {
        this.tryMethod = tryMethod;
    }

    @Override
    public Method getCancelMethod()
    {
        return cancelMethod;
    }

    public void setCancelMethod(Method cancelMethod)
    {
        this.cancelMethod = cancelMethod;
    }

    @Override
    public Method getConfirmMethod()
    {
        return confirmMethod;
    }

    public void setConfirmMethod(Method confirmMethod)
    {
        this.confirmMethod = confirmMethod;
    }

    @Override
    public Class<?> getTryClass()
    {
        return tryClass;
    }

    public void setTryClass(Class<?> tryClass)
    {
        this.tryClass = tryClass;
    }

    @Override
    public Class<?> getConfirmClass()
    {
        return confirmClass;
    }

    public void setConfirmClass(Class<?> confirmClass)
    {
        this.confirmClass = confirmClass;
    }

    @Override
    public Class<?> getCancelClass()
    {
        return cancelClass;
    }

    public void setCancelClass(Class<?> cancelClass)
    {
        this.cancelClass = cancelClass;
    }
}
