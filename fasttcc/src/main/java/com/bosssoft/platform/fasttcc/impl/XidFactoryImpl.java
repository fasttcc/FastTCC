package com.bosssoft.platform.fasttcc.impl;

import com.bosssoft.platform.fasttcc.Xid;
import com.bosssoft.platform.fasttcc.XidFactory;
import com.jfireframework.baseutil.uniqueid.SpringId;
import com.jfireframework.baseutil.uniqueid.Uid;

public class XidFactoryImpl implements XidFactory
{
    Uid uid = SpringId.getInstance();

    @Override
    public Xid createGlobalID()
    {
        return createGlobalID(uid.generateBytes());
    }

    @Override
    public Xid createGlobalID(byte[] globalId)
    {
       return createXid(globalId,null);
    }

    @Override
    public Xid createBranchId(byte[] globalId)
    {
        return createXid(globalId, uid.generateBytes());
    }

    @Override
    public Xid createXid(byte[] globalId, byte[] branchId)
    {
        XidImpl xid = new XidImpl();
        xid.setGlobalId(globalId);
        xid.setBrachId(branchId);
        return xid;
    }
}
