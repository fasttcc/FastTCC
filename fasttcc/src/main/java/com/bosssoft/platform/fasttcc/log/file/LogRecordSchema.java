package com.bosssoft.platform.fasttcc.log.file;

public enum LogRecordSchema
{
    CREATE_TCC_TRANSACTION((byte) 1),//
    REGISTER_lOCAL_TRANSACTION((byte) 2),//
    REGISTER_TCC_INVOKE((byte) 3),//
    REGISTER_REMOTE_RESOURCE((byte) 4), //
    UPDATE_TRANSACTION_STATE((byte) 5);
    private byte value;

    LogRecordSchema(byte value)
    {
        this.value = value;
    }

    public byte schema()
    {
        return value;
    }

    public static LogRecordSchema valueOf(byte value)
    {
        switch (value)
        {
            case 1:
                return CREATE_TCC_TRANSACTION;
            case 2:
                return REGISTER_lOCAL_TRANSACTION;
            case 3:
                return REGISTER_TCC_INVOKE;
            case 4:
                return REGISTER_REMOTE_RESOURCE;
            case 5:
                return UPDATE_TRANSACTION_STATE;
            default:
                throw new IllegalArgumentException();
        }
    }
}
