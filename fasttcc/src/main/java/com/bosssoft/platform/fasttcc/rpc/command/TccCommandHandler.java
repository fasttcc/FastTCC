package com.bosssoft.platform.fasttcc.rpc.command;

import com.bosssoft.platform.fasttcc.support.OriginMethodInvoke;
import com.bosssoft.platform.fasttcc.TccTransaction;
import com.bosssoft.platform.fasttcc.TccTransactionManager;

public interface TccCommandHandler
{
    void setTccTransactionManager(TccTransactionManager manager);

    Object processTccMethodCallCommand(TccMethodCallCommand command, OriginMethodInvoke originMethodInvoke) throws Exception;

    void processTccCommitCommand(TccCommitCommand command) throws Exception;

    void processTccRollbackCommand(TccRollbackCommand command) throws Exception;

    void addTransaction(TccTransaction tccTransaction);
}
