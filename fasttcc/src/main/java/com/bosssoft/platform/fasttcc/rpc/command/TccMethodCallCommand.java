package com.bosssoft.platform.fasttcc.rpc.command;

import com.bosssoft.platform.fasttcc.Xid;

public class TccMethodCallCommand
{
    private Xid    xid;
    private String nodeIdentifier;
    private String callId;

    public Xid getXid()
    {
        return xid;
    }

    public void setXid(Xid xid)
    {
        this.xid = xid;
    }

    public String getNodeIdentifier()
    {
        return nodeIdentifier;
    }

    public void setNodeIdentifier(String nodeIdentifier)
    {
        this.nodeIdentifier = nodeIdentifier;
    }

    public String getCallId()
    {
        return callId;
    }

    public void setCallId(String callId)
    {
        this.callId = callId;
    }
}
