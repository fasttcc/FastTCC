package com.bosssoft.platform.fasttcc.rpc.command;

import com.bosssoft.platform.fasttcc.Xid;

public class TccRollbackCommand
{
    private Xid    xid;
    private String nodeIdentifier;

    public Xid getXid()
    {
        return xid;
    }

    public void setXid(Xid xid)
    {
        this.xid = xid;
    }

    public String getNodeIdentifier()
    {
        return nodeIdentifier;
    }

    public void setNodeIdentifier(String nodeIdentifier)
    {
        this.nodeIdentifier = nodeIdentifier;
    }
}
