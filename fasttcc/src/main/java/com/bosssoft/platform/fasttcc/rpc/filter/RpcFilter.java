package com.bosssoft.platform.fasttcc.rpc.filter;

import com.bosssoft.platform.fasttcc.rpc.command.TccMethodCallCommand;

public interface RpcFilter
{
    /**
     * 在发送远程请求前执行拦截，如果当前是在TCC事务的作用域下并且在尝试阶段，则会返回一个TccMethodCallCommand。其余情况返回null，可以忽略
     *
     * @return
     */
    TccMethodCallCommand beforeSendRequest(String remoteIdentifier);
}
