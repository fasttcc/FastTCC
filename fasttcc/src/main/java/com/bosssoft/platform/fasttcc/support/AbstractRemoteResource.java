package com.bosssoft.platform.fasttcc.support;

import com.bosssoft.platform.fasttcc.CompleteStageListener;
import com.bosssoft.platform.fasttcc.RemoteResource;
import com.bosssoft.platform.fasttcc.Xid;
import com.jfireframework.baseutil.TRACEID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractRemoteResource implements RemoteResource
{

    protected final      String identifier;
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRemoteResource.class);
    protected            String ip;
    protected            String port;
    protected            String commitUrlPrefix;
    protected            String rollbackUrlPrefix;

    public AbstractRemoteResource(String identifier)
    {
        this.identifier = identifier;
        LOGGER.debug("traceId:{} 远端标识符:{}", TRACEID.currentTraceId(), identifier);
        int first = identifier.indexOf(':');
        ip = identifier.substring(0, first);
        int second = identifier.indexOf(':', first + 1);
        port = identifier.substring(first + 1, second);
        commitUrlPrefix = "http://" + ip + ":" + port + "/fasttcc/commit/";
        rollbackUrlPrefix = "http://" + ip + ":" + port + "/fasttcc/rollback/";
    }

    @Override
    public boolean isSameInstance(RemoteResource remoteResource)
    {
        if (identifier.equals(remoteResource.getIdentifier()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }
}
