package com.bosssoft.platform.fasttcc.support;

public interface BeanClassFinder
{
    Class<?> find(String beanName);
}
