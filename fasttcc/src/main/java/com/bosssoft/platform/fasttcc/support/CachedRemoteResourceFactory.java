package com.bosssoft.platform.fasttcc.support;

import com.bosssoft.platform.fasttcc.RemoteResource;
import com.bosssoft.platform.fasttcc.RemoteResourceFactory;

import java.util.concurrent.ConcurrentHashMap;

public abstract class CachedRemoteResourceFactory implements RemoteResourceFactory
{
    protected ConcurrentHashMap<String, RemoteResource> store = new ConcurrentHashMap<>();

    @Override
    public RemoteResource get(String identifier)
    {
        RemoteResource remoteResource = store.get(identifier);
        if (remoteResource == null)
        {
            remoteResource = build(identifier);
            store.putIfAbsent(identifier, remoteResource);
            remoteResource = store.get(identifier);
        }
        return remoteResource;
    }

    protected abstract RemoteResource build(String identifier);
}
