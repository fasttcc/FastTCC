package com.bosssoft.platform.fasttcc.support;

import java.lang.reflect.Method;

public interface CompleteStageHelper
{
    void invokeMethod(Method method, Class<?> ckass, Object[] params);
}
