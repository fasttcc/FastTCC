package com.bosssoft.platform.fasttcc.support;

import java.lang.reflect.Method;

public interface JdbcTransactionInfoDetect
{
    enum TransactionBehavior
    {
        NONE,
        /**
         * 如果存在一个事务，则支持当前事务。如果没有事务则开启一个新的事务
         */
        PROPAGATION_REQUIRED,
        /**
         * 如果存在一个事务，支持当前事务。如果没有事务，则非事务的执行。
         */
        PROPAGATION_SUPPORTS,
        /**
         * 如果已经存在一个事务，支持当前事务。如果没有一个活动的事务，则抛出异常
         */
        PROPAGATION_MANDATORY,
        /**
         * 总是开启一个新的事务。如果一个事务已经存在，则将这个存在的事务挂起。
         */
        PROPAGATION_REQUIRES_NEW,
        /**
         * 总是非事务地执行，并挂起任何存在的事务。
         */
        PROPAGATION_NOT_SUPPORTED,
        /**
         * 总是非事务地执行，如果存在一个活动事务，则抛出异常
         */
        PROPAGATION_NEVER,
        /**
         * 如果一个活动的事务存在，则运行在一个嵌套的事务中. 如果没有活动事务, 则按TransactionDefinition.PROPAGATION_REQUIRED 属性执行
         */
        PROPAGATION_NESTED,
    }

    /**
     * 检查方法信息，返回该方法上标记是事务传播行为
     *
     * @param method
     * @return
     */
    TransactionBehavior detect(Method method);
}
