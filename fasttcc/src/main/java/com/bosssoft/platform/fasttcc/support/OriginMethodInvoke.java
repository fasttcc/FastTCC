package com.bosssoft.platform.fasttcc.support;

public interface OriginMethodInvoke
{
    /**
     * 执行原始方法
     */
    Object invoke();
}
