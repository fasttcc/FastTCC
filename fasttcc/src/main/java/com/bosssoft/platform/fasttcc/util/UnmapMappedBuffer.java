package com.bosssoft.platform.fasttcc.util;

import com.jfireframework.baseutil.reflect.ReflectUtil;
import sun.nio.ch.FileChannelImpl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.MappedByteBuffer;

public class UnmapMappedBuffer
{
    private static Method unmapMethod;

    static
    {
        try
        {
            unmapMethod = FileChannelImpl.class.getDeclaredMethod("unmap", MappedByteBuffer.class);
        }
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
        }
        unmapMethod.setAccessible(true);
    }

    public static void unmap(MappedByteBuffer buffer)
    {
        try
        {
            unmapMethod.invoke(FileChannelImpl.class, buffer);
        }
        catch (IllegalAccessException e)
        {
            ReflectUtil.throwException(e);
        }
        catch (InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }
}
